//=============================================================================================
// Szamitogepes grafika hazi feladat keret. Ervenyes 2014-tol.
// A //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// sorokon beluli reszben celszeru garazdalkodni, mert a tobbit ugyis toroljuk.
// A beadott program csak ebben a fajlban lehet, a fajl 1 byte-os ASCII karaktereket tartalmazhat.
// Tilos:
// - mast "beincludolni", illetve mas konyvtarat hasznalni
// - faljmuveleteket vegezni (printf is fajlmuvelet!)
// - new operatort hivni az onInitialization függvényt kivéve, a lefoglalt adat korrekt felszabadítása nélkül
// - felesleges programsorokat a beadott programban hagyni
// - tovabbi kommenteket a beadott programba irni a forrasmegjelolest kommentjeit kiveve
// ---------------------------------------------------------------------------------------------
// A feladatot ANSI C++ nyelvu forditoprogrammal ellenorizzuk, a Visual Studio-hoz kepesti elteresekrol
// es a leggyakoribb hibakrol (pl. ideiglenes objektumot nem lehet referencia tipusnak ertekul adni)
// a hazibeado portal ad egy osszefoglalot.
// ---------------------------------------------------------------------------------------------
// A feladatmegoldasokban csak olyan gl/glu/glut fuggvenyek hasznalhatok, amelyek
// 1. Az oran a feladatkiadasig elhangzottak ES (logikai AND muvelet)
// 2. Az alabbi listaban szerepelnek:
// Rendering pass: glBegin, glVertex[2|3]f, glColor3f, glNormal3f, glTexCoord2f, glEnd, glDrawPixels
// Transzformaciok: glViewport, glMatrixMode, glLoadIdentity, glMultMatrixf, gluOrtho2D,
// glTranslatef, glRotatef, glScalef, gluLookAt, gluPerspective, glPushMatrix, glPopMatrix,
// Illuminacio: glMaterialfv, glMaterialfv, glMaterialf, glLightfv
// Texturazas: glGenTextures, glBindTexture, glTexParameteri, glTexImage2D, glTexEnvi,
// Pipeline vezerles: glShadeModel, glEnable/Disable a kovetkezokre:
// GL_LIGHTING, GL_NORMALIZE, GL_DEPTH_TEST, GL_CULL_FACE, GL_TEXTURE_2D, GL_BLEND, GL_LIGHT[0..7]
//
// NYILATKOZAT
// ---------------------------------------------------------------------------------------------
// Nev    : <VEZETEKNEV(EK)> <KERESZTNEV(EK)>
// Neptun : <NEPTUN KOD>
// ---------------------------------------------------------------------------------------------
// ezennel kijelentem, hogy a feladatot magam keszitettem, es ha barmilyen segitseget igenybe vettem vagy
// mas szellemi termeket felhasznaltam, akkor a forrast es az atvett reszt kommentekben egyertelmuen jeloltem.
// A forrasmegjeloles kotelme vonatkozik az eloadas foliakat es a targy oktatoi, illetve a
// grafhazi doktor tanacsait kiveve barmilyen csatornan (szoban, irasban, Interneten, stb.) erkezo minden egyeb
// informaciora (keplet, program, algoritmus, stb.). Kijelentem, hogy a forrasmegjelolessel atvett reszeket is ertem,
// azok helyessegere matematikai bizonyitast tudok adni. Tisztaban vagyok azzal, hogy az atvett reszek nem szamitanak
// a sajat kontribucioba, igy a feladat elfogadasarol a tobbi resz mennyisege es minosege alapjan szuletik dontes.
// Tudomasul veszem, hogy a forrasmegjeloles kotelmenek megsertese eseten a hazifeladatra adhato pontokat
// negativ elojellel szamoljak el es ezzel parhuzamosan eljaras is indul velem szemben.
//=============================================================================================

#define _USE_MATH_DEFINES
#include <math.h>
#include <stdlib.h>

#if defined(__APPLE__)
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#else
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
#include <windows.h>
#endif
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Innentol modosithatod...

//--------------------------------------------------------
// 3D Vektor
//--------------------------------------------------------

#include <iostream>

const float PI = 3.1415f;

struct Vector {
    float x, y, z;

    Vector( ) {
        x = y = z = 0;
    }
    Vector(float x0, float y0, float z0 = 0) {
        x = x0; y = y0; z = z0;
    }
    Vector operator*(float a) {
        return Vector(x * a, y * a, z * a);
    }
    Vector operator+(const Vector& v) {
        return Vector(x + v.x, y + v.y, z + v.z);
    }
    Vector operator-(const Vector& v) {
        return Vector(x - v.x, y - v.y, z - v.z);
    }
    float operator*(const Vector& v) { 	// dot product
        return (x * v.x + y * v.y + z * v.z);
    }
    Vector operator%(const Vector& v) { 	// cross product
        return Vector(y*v.z-z*v.y, z*v.x - x*v.z, x*v.y - y*v.x);
    }
    float Length() { return sqrt(x * x + y * y + z * z); }
};

inline Vector Normalize(Vector v) { return v * (1 / v.Length()); }

//--------------------------------------------------------
// Spektrum illetve szin
//--------------------------------------------------------
struct Color {
    float r, g, b;

    Color( ) {
        r = g = b = 0;
    }
    Color(float r0, float g0, float b0) {
        r = r0; g = g0; b = b0;
    }
    Color operator*(float a) {
        return Color(r * a, g * a, b * a);
    }
    Color operator*(const Color& c) {
        return Color(r * c.r, g * c.g, b * c.b);
    }
    Color operator+(const Color& c) {
        return Color(r + c.r, g + c.g, b + c.b);
    }
};

const int screenWidth = 600;	// alkalmazás ablak felbontása
const int screenHeight = 600;
long i_time, d_time;
long cam_time;

void setCamera() {
    gluLookAt(-4 /** cos(cam_time)*/, 3, -30 /** sin(cam_time)*/, 0, 0, 0, 0, 1, 0);
}

GLuint texture[1];

struct Cube {
    const Vector s;
    Vector A, B, C, D, E, F, G, H;
    Cube(const Vector& s) : A(s.x, s.y, -s.z), B(s.x, s.y, s.z), C(s.x, -s.y, s.z), D(s.x, -s.y, -s.z),
                            E(-s.x, s.y, -s.z), F(-s.x, s.y, s.z), G(-s.x, -s.y, s.z), H(-s.x, -s.y, -s.z) {}
    void glQuad(Vector& a, Vector& b, Vector& c, Vector& d) {
        Vector normal = Normalize(b-a % c-a);
        glColor3f(fabs(normal.x), fabs(normal.y), fabs(normal.z));
        glNormal3f(normal.x, normal.y, normal.z);
        glVertex3f(a.x, a.y, a.z); glVertex3f(b.x, b.y, b.z); glVertex3f(c.x, c.y, c.z); glVertex3f(d.x, d.y, d.z);
    }
    void drawCube() {
        glBegin(GL_QUADS);
        glQuad(A, B, C, D); glQuad(E, H, G, F); glQuad(A, E, F, B);
        glQuad(D, C, G, H); glQuad(B, F, G, C); glQuad(A, D, H, E);
        glEnd();

    }
};

struct Bezier {
    Vector cps[7];
    Bezier(Vector& v0, Vector& v1, Vector& v2, Vector& v3, Vector& v4, Vector& v5, Vector& v6) {
        cps[0] = v0; cps[1] = v1; cps[2] = v2; cps[3] = v3; cps[4] = v4; cps[5] = v5; cps[6] = v6;
    }
    Bezier() {}
    float B(int i, float t) {
        int n = 6 - 1;
        float choose = 1;
        for(int j = 1; j <= i; j++) choose *= (float)(n-j+1)/j;
        return choose * (float)pow(t, i) * (float)pow(1-t, n-i);
    }
    Vector r(float t) {
        Vector rr(0.0f, 0.0f, 0.0f);
        for(int i = 0; i < 6; i++) rr = rr + cps[i] * B(i,t);
        return rr;
    }
    void drawBezier() {
            glBegin(GL_TRIANGLES);
            float t0 = 0.0f;
            for(float t=0.01f; t<=1; t+=0.01) {
                Vector v0 = r(t0);
                Vector v1 = r(t);
                for(float a = 0.0f; a < PI * 2.0f; a += 0.4f) {
                    Vector p1(v1.x * (float)cos(a) + v1.z * (float)sin(a),
                              v1.y,
                              -v1.x * (float)sin(a) + v1.z * (float)cos(a));
                    Vector p2(v0.x * (float)cos(a) + v0.z * (float)sin(a),
                              v0.y,
                              -v0.x * (float)sin(a) + v0.z * (float)cos(a));
                    Vector p3(v1.x * (float)cos(a + 0.4f) + v1.z * (float)sin(a + 0.4f),
                              v1.y,
                              -v1.x * (float)sin(a + 0.4f) + v1.z * (float)cos(a + 0.4f));
                    Vector p4(v0.x * (float)cos(a + 0.4f) + v0.z * (float)sin(a + 0.4f),
                              v0.y,
                              -v0.x * (float)sin(a + 0.4f) + v0.z * (float)cos(a + 0.4f));

                    Vector normal = Normalize(p2-p1 % p3-p1);
                    glNormal3f(normal.x, normal.y, normal.z);
                    glVertex3f(p1.x, p1.y, p1.z);
                    glVertex3f(p2.x, p2.y, p2.z);
                    glVertex3f(p3.x, p3.y, p3.z);
                    glVertex3f(p2.x, p2.y, p2.z);
                    glVertex3f(p3.x, p3.y, p3.z);
                    glVertex3f(p4.x, p4.y, p4.z);

                }
                t0 = t;
            }
            glEnd();
    }
};

struct Field {
    Vector A, B, C, D;
    Vector normal;
    Field(Vector& a, Vector& b, Vector& c, Vector& d) : A(a), B(b), C(c), D(d),
                                                        normal(Normalize(B - A % C - A) * -1.0f) {}
    void drawFiled() {
        glEnable(GL_TEXTURE_2D);

        glBindTexture(GL_TEXTURE_2D, texture[0]);

        glBegin(GL_TRIANGLES);
        glNormal3f(normal.x, normal.y, normal.z);
        glTexCoord2f(0, 0); glVertex3f(A.x, A.y, A.z);
        glTexCoord2f(1, 0); glVertex3f(B.x, B.y, B.z);
        glTexCoord2f(1, 1); glVertex3f(C.x, C.y, C.z);
        glTexCoord2f(1, 0); glVertex3f(B.x, B.y, B.z);
        glTexCoord2f(1, 1); glVertex3f(C.x, C.y, C.z);
        glTexCoord2f(0, 1); glVertex3f(D.x, D.y, D.z);
        glEnd();
        glDisable(GL_TEXTURE_2D);

        glBindTexture(GL_TEXTURE_2D, 0);
    }

};

struct Cylinder {
    float R;
    float l;
    Cylinder(float r, float l) : R(r), l(l) {}
    Cylinder() {}
    void drawCylinder() {
        glBegin(GL_TRIANGLES);
        for(float i = 0; i < 2.0f * PI; i += 0.2f)
            for (float j = 0; j < l; j += 0.2f) {
                Vector p1(R * (float)cos(i), R * (float)sin(i), j);
                Vector p2(R * (float)cos(i), R * (float)sin(i), j + 0.2f);
                Vector p3(R * (float)cos(i + 0.2f), R * (float)sin(i + 0.2f), j);
                Vector p4(R * (float)cos(i + 0.2f), R * (float)sin(i + 0.2f), j + 0.2f);
                Vector normal = Normalize(p3-p1 % p2-p1);
                glNormal3f(normal.x, normal.y, normal.z);
                glVertex3f(p1.x, p1.y, p1.z);
                glVertex3f(p2.x, p2.y, p2.z);
                glVertex3f(p3.x, p3.y, p3.z);
                glVertex3f(p2.x, p2.y, p2.z);
                glVertex3f(p3.x, p3.y, p3.z);
                glVertex3f(p4.x, p4.y, p4.z);
            }
        glEnd();
    }
};

struct Sphere {
    float R;
    Sphere(float r) : R(r) {}
    Sphere() {}
    void drawSphere() {
        glBegin(GL_TRIANGLES);
        for(float i = 0; i < 2.0f * PI; i += 0.2f) {
            for (float j = 0; j < 2.0f * PI; j += 0.2f) {
                Vector s_point(R  * (float) sin(i) * (float) cos(j),
                        R * (float) sin(i) * (float) sin(j),
                        R * (float) cos(i));
                Vector normal = Normalize(s_point - Vector(0.0f, 0.0f, 0.0f));
                glNormal3f(normal.x, normal.y, normal.z);
                glVertex3f(R * (float) sin(i) * (float) cos(j),
                           R * (float) sin(i) * (float) sin(j),
                           R * (float) cos(i));
                glVertex3f(R * (float) sin(i) * (float) cos(j + 0.2f),
                           R * (float) sin(i) * (float) sin(j + 0.2f),
                           R * (float) cos(i));
                glVertex3f(R * (float) sin(i + 0.2f) * (float) cos(j),
                           R * (float) sin(i + 0.2f) * (float) sin(j),
                           R * (float) cos(i + 0.2f));
                glVertex3f(R * (float) sin(i) * (float) cos(j + 0.2f),
                           R * (float) sin(i) * (float) sin(j + 0.2f),
                           R * (float) cos(i));
                glVertex3f(R * (float) sin(i + 0.2f) * (float) cos(j),
                           R * (float) sin(i + 0.2f) * (float) sin(j),
                           R * (float) cos(i + 0.2f));
                glVertex3f(R * (float) sin(i + 0.2f) * (float) cos(j + 0.2f),
                           R * (float) sin(i + 0.2f) * (float) sin(j + 0.2f),
                           R * (float) cos(i + 0.2f));
            }
        }
        glEnd();
    }
};

struct Csirguru {
    Bezier body;
    Cylinder neck;
    Sphere head;
    float rot_x;
    Vector trans;
    Csirguru(Bezier& b, Cylinder& neck, Sphere& s, float rot_x) : body(b), neck(neck), head(s), rot_x(rot_x), trans() {}
    Csirguru() : body(), neck(), head(), rot_x() {}
    void drawCs() {
        glPushMatrix();
        glRotatef(rot_x, 1.0f, 0.0f, 0.0f);
        glTranslatef(trans.x, trans.y, trans.z);
            glPushMatrix();
            glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
            glTranslatef(0.0f, 0.0f, 0.0f);
            glColor3f(1.0f, 1.0f, 1.0f);
            body.drawBezier();
            glPopMatrix();

            glPushMatrix();
            glRotatef(81.0f, 1.0f, 0.0f, 0.0f);
            glTranslatef(-0.7f, 0.0f, -0.7f);
            glColor3f(1.0f, 1.0f, 1.0f);
            neck.drawCylinder();
            glPopMatrix();

            glPushMatrix();
            glTranslatef(-0.7f, 0.8f, -0.1f);
            glColor3f(1.0f, 1.0f, 1.0f);
            head.drawSphere();
            glPopMatrix();
        glPopMatrix();
    }

};

const Vector d_size (0.5f, 0.5f, 0.5f);
Cube cube(d_size);
Vector f_a(20.0f, -0.5f, 20.0f); Vector f_b(-20.0f, -0.5f, 20.0f);
Vector f_c(20.0f, -0.5f, -20.0f); Vector f_d(-20.0f, -0.5f, -20.0f);
Field field(f_a, f_b, f_c, f_d);
Vector b_v0(0.25f, 0.0f, 0.25f); Vector b_v1(0.26f, 0.4f, 0.25f); Vector b_v2(0.29f, 0.8f, 0.25f);
Vector b_v3(0.45f, 1.0f, 0.25f); Vector b_v4(0.55f, 1.2f, 0.25f); Vector b_v5(0.25f, 1.4f, 0.25f);
Vector b_v6(0.28f, 1.8f, 0.25f);
Bezier bezier(b_v0, b_v1, b_v2, b_v3, b_v4, b_v5, b_v6);
Sphere sphere(0.5f);
Vector cs_b_v0(0.0f, -1.2f, 0.0f); Vector cs_b_v1(0.1f, -0.8f, 0.2f); Vector cs_b_v2(0.2f, -0.4f, 0.4f);
Vector cs_b_v3(0.3f, 0.0f, 0.6f); Vector cs_b_v4(0.4f, 0.4f, 0.8f); Vector cs_b_v5(0.2f, 0.8f, 0.1f);
Vector cs_b_v6(0.0f, 1.2f, 0.0f);
Bezier body(cs_b_v0, cs_b_v1, cs_b_v2, cs_b_v3, cs_b_v4, cs_b_v5, cs_b_v6);
Cylinder neck(0.1f, 0.65f);
Sphere head(0.2f);

Csirguru csirguru0(body, neck, head, 0.0f); Csirguru csirguru1(body, neck, head, 0.0f);
Csirguru csirguru2(body, neck, head, 0.0f); Csirguru csirguru3(body, neck, head, 0.0f);
Csirguru csirguru4(body, neck, head, 0.0f); Csirguru csirguru5(body, neck, head, 0.0f);
Csirguru csirguru6(body, neck, head, 0.0f); Csirguru csirguru7(body, neck, head, 0.0f);
Csirguru csirguru8(body, neck, head, 0.0f); Csirguru csirguru9(body, neck, head, 0.0f);

float v0 = 3.0f;
//Vector csirguru_trans(0.0f, 0.0f, 0.0f);

// Inicializacio, a program futasanak kezdeten, az OpenGL kontextus letrehozasa utan hivodik meg (ld. main() fv.)
void onInitialization( ) {
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    //glFrontFace(GL_CCW); // Az normál irányából nézve CCW a körüljárási irány
    //glCullFace(GL_BACK); // A hátsó oldalt akarjuk eldobni
    //glEnable(GL_CULL_FACE); // És engedélyezzük a lapeldobást.
    glMatrixMode(GL_PROJECTION);
    glViewport(0, 0, screenWidth, screenHeight);
    gluPerspective(60, 1, 0.1, 50);

    glEnable(GL_DEPTH_TEST);

    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);
    GLfloat p[4] = {-0.0f, 0.0f, -0.0f, 1};
    glLightfv(GL_LIGHT0, GL_POSITION, p);

    glGenTextures(1, texture);

    GLubyte tex_data[1][1024][3];

    for(register unsigned int i = 0; i < 1024; i++) {
        tex_data[0][i][0] = (unsigned char) 0;
        tex_data[0][i][1] = (unsigned char) rand() % 230 + 100;
        tex_data[0][i][2] = (unsigned char) 0;
    }


    glBindTexture(GL_TEXTURE_2D, texture[0]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, 32, 32, 0, GL_RGB, GL_UNSIGNED_BYTE, tex_data);
    glBindTexture(GL_TEXTURE_2D, 0);


}

// Rajzolas, ha az alkalmazas ablak ervenytelenne valik, akkor ez a fuggveny hivodik meg
void onDisplay( ) {
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);		// torlesi szin beallitasa
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // kepernyo torles

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    setCamera();

    field.drawFiled();

    if (i_time / 1000 > 0)
        csirguru0.drawCs();
    if (i_time / 1000 > 1)
        csirguru1.drawCs();
    if (i_time / 1000 > 2)
        csirguru2.drawCs();
    if (i_time / 1000 > 3)
        csirguru3.drawCs();
    if (i_time / 1000 > 4)
        csirguru4.drawCs();
    if (i_time / 1000 > 5)
        csirguru5.drawCs();
    if (i_time / 1000 > 6)
        csirguru6.drawCs();
    if (i_time / 1000 > 7)
        csirguru7.drawCs();
    if (i_time / 1000 > 8)
        csirguru8.drawCs();
    if (i_time / 1000 > 9)
        csirguru9.drawCs();

    //for(int i = 0; i < i_time / 1000; i++)
    //csirguru.drawCs();

    glutSwapBuffers();     				// Buffercsere: rajzolas vege


}

// Billentyuzet esemenyeket lekezelo fuggveny (lenyomas)
void onKeyboard(unsigned char key, int x, int y) {
    if (key == 'd') glutPostRedisplay( ); 		// d beture rajzold ujra a kepet

}

// Billentyuzet esemenyeket lekezelo fuggveny (felengedes)
void onKeyboardUp(unsigned char key, int x, int y) {

}

// Eger esemenyeket lekezelo fuggveny
void onMouse(int button, int state, int x, int y) {
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)   // A GLUT_LEFT_BUTTON / GLUT_RIGHT_BUTTON illetve GLUT_DOWN / GLUT_UP
        glutPostRedisplay( ); 						 // Ilyenkor rajzold ujra a kepet
}

// Eger mozgast lekezelo fuggveny
void onMouseMotion(int x, int y) {

}

float f_t = 0;

void animate_cps(long p_time, long e_time) {
    for(long i = p_time; i < e_time; i++) {
       float j = i / 1000.0f;
        if (i_time / 1000 > 0) {
            csirguru0.trans.x = -v0 * j * (float)sin(PI / 2.0f);
            csirguru0.trans.y = v0 * (j - f_t) * (float)sin(PI / 2.0f) - 5.0f * (j - f_t) * (j - f_t);
            csirguru0.trans.z = 0.0f;
            if(csirguru0.trans.y < 0.0f)
                f_t = j;
        }
        if (i_time / 1000 > 1) {
            csirguru1.trans.x = -v0 * (j - 1.0f) * (float)sin(PI / 2.0f);
            csirguru1.trans.y = v0 * (j - f_t) * (float)sin(PI / 2.0f) - 5.0f * (j - f_t) * (j - f_t);
            csirguru1.trans.z = 2.0f;
            if(csirguru1.trans.y < 0.0f)
                f_t = j;
        }
        if (i_time / 1000 > 2) {
            csirguru2.trans.x = -v0 * (j - 2.0f) * (float)sin(PI / 2.0f);
            csirguru2.trans.y = v0 * (j - f_t) * (float)sin(PI / 2.0f) - 5.0f * (j - f_t) * (j - f_t);
            csirguru2.trans.z = 4.0f;
            if(csirguru2.trans.y < 0.0f)
                f_t = j;
        }
        if (i_time / 1000 > 3) {
            csirguru3.trans.x = -v0 * (j - 3.0f) * (float)sin(PI / 2.0f);
            csirguru3.trans.y = v0 * (j - f_t) * (float)sin(PI / 2.0f) - 5.0f * (j - f_t) * (j - f_t);
            csirguru3.trans.z = 6.0f;
            if(csirguru3.trans.y < 0.0f)
                f_t = j;
        }
        if (i_time / 1000 > 4) {
            csirguru4.trans.x = -v0 * (j - 4000.0f) * (float)sin(PI / 2.0f);
            csirguru4.trans.y = v0 * (j - f_t) * (float)sin(PI / 2.0f) - 5.0f * (j - f_t) * (j - f_t);
            csirguru4.trans.z = 8.0f;
            if(csirguru4.trans.y < 0.0f)
                f_t = j;
        }
        if (i_time / 1000 > 5) {
            csirguru5.trans.x = -v0 * (j - 5.0f) * (float)sin(PI / 2.0f);
            csirguru5.trans.y = v0 * (j - f_t) * (float)sin(PI / 2.0f) - 5.0f * (j - f_t) * (j - f_t);
            csirguru5.trans.z = 10.0f;
            if(csirguru5.trans.y < 0.0f)
                f_t = j;
        }
        if (i_time / 1000 > 6) {
            csirguru6.trans.x = -v0 * (j - 6.0f) * (float)sin(PI / 2.0f);
            csirguru6.trans.y = v0 * (j - f_t) * (float)sin(PI / 2.0f) - 5.0f * (j - f_t) * (j - f_t);
            csirguru6.trans.z = 14.0f;
            if(csirguru6.trans.y < 0.0f)
                f_t = j;
        }
        if (i_time / 1000 > 7) {
            csirguru7.trans.x = -v0 * (j - 7.0f) * (float)sin(PI / 2.0f);
            csirguru7.trans.y = v0 * (j - f_t) * (float)sin(PI / 2.0f) - 5.0f * (j - f_t) * (j - f_t);
            csirguru7.trans.z = 18.0f;
            if(csirguru7.trans.y < 0.0f)
                f_t = j;
        }
        if (i_time / 1000 > 8) {
            csirguru8.trans.x = -v0 * (j - 8.0f) * (float)sin(PI / 2.0f);
            csirguru8.trans.y = v0 * (j - f_t) * (float)sin(PI / 2.0f) - 5.0f * (j - f_t) * (j - f_t);
            csirguru8.trans.z = 22.0f;
            if(csirguru8.trans.y < 0.0f)
                f_t = j;
        }
        if (i_time / 1000 > 9) {
            csirguru9.trans.x = -v0 * (j - 9.0f) * (float)sin(PI / 2.0f);
            csirguru9.trans.y = v0 * (j - f_t) * (float)sin(PI / 2.0f) - 5.0f * (j - f_t) * (j - f_t);
            csirguru9.trans.z = 30.0f;
            if(csirguru9.trans.y < 0.0f)
                f_t = j;
        }

    }
    glutPostRedisplay();

}

void SimulateWorld(long old_tim, long tim) {
    for(long i=old_tim; i<(tim);i++){
        long te = fmin(tim, i + 5);
        animate_cps(i, te);
    }
}

// `Idle' esemenykezelo, jelzi, hogy az ido telik, az Idle esemenyek frekvenciajara csak a 0 a garantalt minimalis ertek
void onIdle( ) {
    d_time = i_time;
    i_time = glutGet(GLUT_ELAPSED_TIME);		// program inditasa ota eltelt ido
    SimulateWorld(d_time, i_time);
}

// ...Idaig modosithatod
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// A C++ program belepesi pontja, a main fuggvenyt mar nem szabad bantani
int main(int argc, char **argv) {
    glutInit(&argc, argv); 				// GLUT inicializalasa
    glutInitWindowSize(600, 600);			// Alkalmazas ablak kezdeti merete 600x600 pixel
    glutInitWindowPosition(100, 100);			// Az elozo alkalmazas ablakhoz kepest hol tunik fel
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);	// 8 bites R,G,B,A + dupla buffer + melyseg buffer

    glutCreateWindow("Grafika hazi feladat");		// Alkalmazas ablak megszuletik es megjelenik a kepernyon

    glMatrixMode(GL_MODELVIEW);				// A MODELVIEW transzformaciot egysegmatrixra inicializaljuk
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);			// A PROJECTION transzformaciot egysegmatrixra inicializaljuk
    glLoadIdentity();

    onInitialization();					// Az altalad irt inicializalast lefuttatjuk

    glutDisplayFunc(onDisplay);				// Esemenykezelok regisztralasa
    glutMouseFunc(onMouse);
    glutIdleFunc(onIdle);
    glutKeyboardFunc(onKeyboard);
    glutKeyboardUpFunc(onKeyboardUp);
    glutMotionFunc(onMouseMotion);

    glutMainLoop();					// Esemenykezelo hurok

    return 0;
}

